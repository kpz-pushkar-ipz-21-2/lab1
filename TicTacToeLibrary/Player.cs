﻿namespace TicTacToeLibrary
{
    public class Player
    {
        protected struct Core
        {
            public int X;
            public int O;
            public int TotalGames;

            public Core()
            {
                X = 0;
                O = 0;
                TotalGames = 0;
            }
        }

        protected int Number;
        protected string Role;

        public Player(int playerNumber, string checkRole = "")
        {
            Number = playerNumber;
            Role = EnterPlayerRole(playerNumber, checkRole);
        }

        public static string EnterPlayerRole(int PlayerNumber, string CheckRole = "")
        {
            string Role;

            switch (CheckRole)
            {
                case "X": return "O";
                case "O": return "X";
                default: Console.WriteLine("Existing game roles: X, O."); break;
            }

            do
            {
                Console.Write($"Player {PlayerNumber} role: "); 
                Role = Console.ReadLine();
                if (CheckRole != Role && (Role == "X" || Role == "O"))
                    break;
                else
                {
                    Console.Write("Error! Role is entered wrong. Repeat: "); 
                    Role = Console.ReadLine();
                }
            } while (CheckRole == Role && (Role == "X" || Role == "O"));

            return Role;
        }

        public void SetRole(string NewRole)
        {
            if (NewRole != "X" && NewRole != "O")
                return;

            Role = NewRole;
        }
        public string GetRole() => Role;
    }
}
